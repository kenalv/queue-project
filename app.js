var azure = require('azure-storage');
var express = require('express');
const Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var app = express();
const TYPES = require('tedious').TYPES;

var bodyParser = require('body-parser');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

const config = require('./config');
var configBD = {
  userName: 'administrador', // update me
  password: 'Adm1n1strador', // update me
  server: 'queue-project.database.windows.net',
  options: {
    database: 'queue-project',
    encrypt: true,
  }
}

const queueSvc = azure.createQueueService('soperativos','Lb8iaKZZ1a+REAB4Uxp0/xYsE32BsJeYu8DRa5Tq1uBaHiwgL8n4UG203Zp2mnIumhJmCjp36NfG3jQeWHhr1w==');




/*
queueSvc.createQueueIfNotExists('myqueue', function(error, results, response){
  if(!error){
    // Queue created or exists
  }
});
*/

queueSvc.createMessage('myqueue', JSON.stringify({'name':"kenneth"}), function(error, results, response){
  if(!error){
    console.log("HOLAAA");
  }

});


app.get('/', function (req, res) {
  //Get message test from queue
  queueSvc.getMessages('myqueue', function(error, results, response){
    if(!error){
      // Message text is in results[0].messageText
      console.log(results);
      var message = results[0];
      queueSvc.deleteMessage('myqueue', message.messageId, message.popReceipt, function(error, response){
        if(!error){
          //returns message on the queue and show the data
          res.send(message);

          }
      });
    }
  });
});

app.get('/weather/get/:countrycode',function(req,res){
    let data =  req.params;
    queueSvc.setQueueMetadata('myqueue',data, function(error, results, response){
      if(!error){
        //Message was created
        res.send(response);
      }
    });
});

app.post('/weather/push/city', function(req, res) {

  var connection = new Connection(configBD);
  connection.req = req;
  connection.on('connect', function(err) {
    if (err) {
      console.log(err);
    } else {
    
      var countrycode =connection.req.body.countrycode;
      var countryname = connection.req.body.country;
      var city = connection.req.body.city;
      var longitude = connection.req.body.longitude;
      var latitude = connection.req.body.latitude;
      var temperature  = connection.req.body.temperature;
      var winds = connection.req.body.winds;
      var windir = connection.req.body.windir;
      var weather = connection.req.body.weather;

      request = new Request("EXEC insertCountry @countrycode,@countryname,@city,@longitude,@latitude,@temperature,@winds,@windir,@weather", function(err, rowCount) {
        console.log("ENTRO esa picha de REQUEST");
        if (err) {
          console.log(err);
        } else {
          console.log("SIRVo esa picha de INSERT");
          connection.close();
        }
      });
      request.addParameter('countrycode', TYPES.Int, countrycode);
      request.addParameter('countryname', TYPES.VarChar, countryname);
      request.addParameter('city', TYPES.VarChar, city);
      request.addParameter('longitude', TYPES.Float, longitude);
      request.addParameter('latitude', TYPES.Float, latitude);
      request.addParameter('temperature', TYPES.Float, temperature);
      request.addParameter('winds', TYPES.Float, winds);
      request.addParameter('windir', TYPES.Float, windir);
      request.addParameter('weather', TYPES.VarChar, weather);
     
      request.on('doneProc', function (rowCount, more, returnStatus, rows) { });
      connection.execSql(request); 
    }
  });
  res.send("lol");
});

app.listen(config.app.port, function () {
  console.log('Example app listening on port ' + config.app.port);
});